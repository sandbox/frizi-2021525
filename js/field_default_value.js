(function($){
/**
 * Toggle the description of the default - descriptive form fields value.
 */

function fdvInObject(srchValue, tmpObj) {
	for (x in tmpObj){
		if(tmpObj[x] == srchValue || x == srchValue){
			return true;
		}
	}
	return false;
}

Drupal.behaviors.field_default_value = {
  attach: function (context, settings) {
	  $(window).load(function() {

		if (typeof Drupal.settings.webformDefault != 'undefined') {
			// for each entries from array, check if field value is empty, if yes, fill it with the default description
			for (x in Drupal.settings.webformDefault){
				if ($('#edit-submitted-' + x).val() == '' || !$('#edit-submitted-' + x).val()) {
					$('#edit-submitted-' + x).val(Drupal.settings.webformDefault[x]);
				}
			}
			$('.webform-client-form .form-text, .webform-client-form textarea').click(function() {
				if (fdvInObject($(this).val(), Drupal.settings.webformDefault)) {
					 $(this).val('');
				  }
			});

			// on blur refill form
			$('.webform-client-form .form-text, .webform-client-form .form-textarea').blur(function() {
				  if ($(this).val() == '') {
					for (x in Drupal.settings.webformDefault){
						if($(this).attr('id').indexOf(x) >= 0){
							console.log(x);
							$(this).val(Drupal.settings.webformDefault[x]);
							return true;
						}
					}
				  }
			});

			// check if default values are set on submit. If yes, empty the to avoid search by description text
			  $('.webform-client-form .form-submit').click(function() {
				 $(".form-text, .form-textarea").each(function() {
					 if (fdvInObject($(this).val(), Drupal.settings.webformDefault)) {
						 $(this).val('');
					  }
				  });
			  });

			  // change input box font color at typing
			  $("input[type='text'], textarea").keyup(function() {	
				  $(this).css('color','#000');
			  });
		}

		if (typeof Drupal.settings.searchDefault != 'undefined') {
			if ($('[name="search_block_form"], .search-form .form-text').val() == '' || !$('[name="search_block_form"], .search-form .form-text').val()) {
				$('[name="search_block_form"]').val(Drupal.settings.searchDefault);
			}

			$('[name="search_block_form"], .search-form .form-text').click(function() {
				if($(this).val() == Drupal.settings.searchDefault){
					$(this).val('');
				}
			});

			$('[name="search_block_form"], .search-form .form-text').blur(function() {
				if ($(this).val() == '') {
					$(this).val(Drupal.settings.searchDefault);
				}
			});
			
			$('#search-block-form .form-submit, .search-form .form-submit').click(function() {
				
				
				console.log($('[name="search_block_form"]').val());
				var searchField = $(this).parent().find($('[name="search_block_form"]'));

				if ($('[name="search_block_form"]').val() == Drupal.settings.searchDefault) {
					$('[name="search_block_form"]').val('');
				} else if ($('.search-form .form-text').val() == Drupal.settings.searchDefault) {
					$('.search-form .form-text').val('');
				}
			});
		}

	  }); // / Window load
	}
}; // / Drupal behaviors

})(jQuery);